
LOCATION INFORMATION
####################

The install file is located within "/Jaktskyttestigprogram/Debug".

The source code is located within "/Jaktskyttestig/".

The user manual is located within "/Manual/".

INSTALLATION INFORMATION
########################

The installation includes the program, manual, sourcecode and test- file
in the installation folder.