﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WindowsFormsApplication1.GUIForms;
using WindowsFormsApplication1.Models;
using WindowsFormsApplication1.Validators;
using WindowsFormsApplication1.Columnsorter;
using WindowsFormsApplication1.Helpers;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        Logic logic;
        // This variable contains all Participants being displayed in listView1. 
        // To show a different view this variable has to be rewritten with another list of Participants
        // obtained from Logic. After that you do this.updateParticipantList() to update the view.
        List<Participant> currentlyDisplayedParticipants = new List<Participant>();
        //
        Helpers.InteractiveHelper helper = new Helpers.InteractiveHelper();

        
        public Form1()
        {
            InitializeComponent();
            InitializeMyComponents();
            this.logic = new Logic();
            // Andrei: Lägger till kolumner i tabellen
            listView1.Columns.Add("StartNo.", 60, HorizontalAlignment.Left);
            listView1.Columns.Add("Name", 270, HorizontalAlignment.Left);
            listView1.Columns.Add("PersonalNumber", 150, HorizontalAlignment.Left);
            listView1.Columns.Add("PhoneNumber", 150, HorizontalAlignment.Left);
            listView1.Columns.Add("Class", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("Status", 60, HorizontalAlignment.Left);
            listView1.Columns.Add("Result", 60, HorizontalAlignment.Left);
            listView1.View = System.Windows.Forms.View.Details;
            //Andrei: updating menu items
            this.updateMenuItems();

            //Christoffer: Initiate columnsorter and assign it to listView1
            this.listView1.ListViewItemSorter = LVColumnSorter;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            
        }

        public List<Participant> CurrentlyDisplayedParticipants
        {
            get
            {
                return this.currentlyDisplayedParticipants;
            }
            set
            {
                this.currentlyDisplayedParticipants = value;
            }
        }


        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {

            NewCompetitionForm n = new NewCompetitionForm(this.logic, this);
            n.Show();
        }

        private void issueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Andrei: logic ska vara tillgänglig från andra former också
            NewStartCardForm n = new NewStartCardForm(this.logic, this);
            n.Show();            
        }

        private void submitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Andrei: we're creating a form to select a participant from a list of those who haven't submitted their startcards yet
            ChooseParticipantForm c = new ChooseParticipantForm(this.logic, this, this.logic.getNonSubmittedStartCardParticipants());
            c.Show();
            
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void startCardToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }


        // Andrei: den här metoden uppdaterar deltagarlistan
        // Metoden kan kallas från alla andra metoder i klassen när det behövs att uppdatera listan
        // TODO: More columns needed in listView1: e. g. to show current status (Shooting/Submitted), Result
        public void updateParticipantsList()
        {
            while (listView1.Items.Count > 0)
            {
                listView1.Items.RemoveAt(0);
            }

            List<Participant> participants = this.currentlyDisplayedParticipants; // this.logic.Competition.Participants;
            
            for (int i = 0; i < participants.Count; i++)
            {
                string[] listViewRow = new string[7];
                listViewRow[0] = participants[i].StartNumber.ToString();
                listViewRow[1] = participants[i].Name;
                listViewRow[2] = participants[i].PersonalNumber;
                listViewRow[3] = participants[i].PhoneNumber;
                listViewRow[4] = participants[i].CompetitionClass.ToString();

                //Christoffer: Added a column for status
                if (participants[i].SubmittedStartCard)
                {
                    listViewRow[5] = "Done";
                }
                else
                {
                    listViewRow[5] = "Active";
                }
                //Christoffer: Added a result column
                if (participants[i].SubmittedStartCard)
                {
                    listViewRow[6] = participants[i].TotalResult().ToString();
                }
                else
                {
                    listViewRow[6] = "N/A";
                }

                ListViewItem lvi = new ListViewItem(listViewRow);
                listView1.Items.Add(lvi);
            }
        }

        // Andrei: method to update status strip. Status strip increases usability.
        public void updateStatusStrip()
        {
            toolStripStatusLabel1.Text = this.logic.getStatusLine();
        }
        //TODO: Could be possible to add comments for each menu item in the status strip: e. g. when mouse is over the item, a comment is shown what the item does.



        // Andrei: use this method to update menu items when needed
        public void updateMenuItems()
        {
            // Andrei: if no Competition has been created, some menu items are disabled
            // It should not be allowed to issue StartCards
            // It should not be possible to submit any StartCards
            // It should not be possible to view Participant lists

            this.updateStatusStrip();

            // competition has no file and no competition has been created yet
            if (this.logic.CompetitionFile.Equals("") && this.logic.Competition == null)
            {
                this.Text = "Jaktskyttestig - New File";
            }
            // competition has a name and a file
            else if (!this.logic.CompetitionFile.Equals("") && !this.logic.Competition.Name.Equals(""))
            {
                this.Text = "Jaktskyttestig - " + this.logic.Competition.Name + " - " + this.logic.CompetitionFile;
            }
            // competition has only a name, but no file yet
            else if (this.logic.CompetitionFile.Equals("") && this.logic.Competition.Name != null)
            {
                this.Text = "Jaktskyttestig - "+this.logic.Competition.Name;
            }

            // When no Competition is created, startcard menu and save menu item are disabled
            if (this.logic.Competition == null)
            {
                startCardToolStripMenuItem.Enabled = false;
                saveToolStripMenuItem.Enabled = false;
            }
            else
            {
                startCardToolStripMenuItem.Enabled = true;
                saveToolStripMenuItem.Enabled = true;

            }

            // Andrei: View menu is enabled only if a competition is created and if there is at least one participant
            // When all issued startcards are submitted, the submit menu item must be disabled
            if (this.logic.Competition == null || this.logic.Competition.getNumberOfParticipants() == 0)
            {
                viewtoolStripMenuItem.Enabled = false;
            }
            else
            {
                viewtoolStripMenuItem.Enabled = true;
            }

            // Andrei: if no StartCards have been issued yet, it is impossible to submit a card. The option is greyed out.
            if (this.logic.getNonSubmittedStartCardParticipants().Count == 0 || this.logic.getNonSubmittedStartCardParticipants().Count == 0)
            {
                submittedStartCardsToolStripMenuItem.Enabled = false;
            }
            else
            {
                submittedStartCardsToolStripMenuItem.Enabled = true;
            }
            // Andrei: if a Competition is created, start card submit should be deactivated, unless there are issued startcards not submitted yet

            if (this.logic.Competition != null && this.logic.getNonSubmittedStartCardParticipants().Count > 0)
            {
                submitToolStripMenuItem.Enabled = true;
            }
            else
            {
                submitToolStripMenuItem.Enabled = false;
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Andrei: Opening the Open Competition Dialog
            openFileDialog1.Title = "Open a saved competition...";
            // This sets the default directory for the open dialog to the home folder
            openFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog1.FileName = "";
            // showing only *.cmp files in the dialog. It is also possible to choose All files.
            openFileDialog1.Filter = "Competition files (*.cmp)|*.cmp|All Files (*.*)|*.*";
            // The file is only opened if the user has chosen a file and pressed open
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                this.logic.openFile(openFileDialog1.FileName);
                this.currentlyDisplayedParticipants = this.logic.Competition.Participants;
            }
            this.updateMenuItems();
            this.updateParticipantsList();
        }

        //TODO: Save As menu item needed to save competition to a different file than the current

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {

            // The save dialog opens only if no filename has been assigned yet to the current competition
            // When saving an opened competition, it is just saved to the current file.
            if (this.logic.CompetitionFile.Equals("")) {
                // setting title
                saveFileDialog1.Title = "Save competition...";
                // setting view filter: showing only *.cmp files
                saveFileDialog1.Filter = "Competition files (*.cmp)|*.cmp";
                // the default directory is user home folder
                saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                saveFileDialog1.FileName = "";
                // the file is saved only if the user has given a filename and pressed OK:
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    this.logic.saveFile(saveFileDialog1.FileName);
                }

            }
            // saving to the current file, without prompting the user to choose a file
            else {
                this.logic.saveFile(this.logic.CompetitionFile);
            }
            this.updateMenuItems();
            this.updateParticipantsList();
        }


        // Andrei: This method shows Participants who have received a StartCard, but not submitted it back yet.
        private void currentlyShootingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.currentlyDisplayedParticipants = this.logic.getNonSubmittedStartCardParticipants();
            this.updateParticipantsList();
            
        }

        // Andrei: This method shows all Participants regardless of their status
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.currentlyDisplayedParticipants = this.logic.Competition.Participants;
            this.updateParticipantsList();

        }

        private void submittedStartCardsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.currentlyDisplayedParticipants = this.logic.getSubmittedStartCardParticipants();
            this.updateParticipantsList();

        }

        private void openToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.currentlyDisplayedParticipants = this.logic.Competition.getParticipantListByClass(Participant.competitionClassEnum.Open);
            this.updateParticipantsList();
        }

        private void youngerOBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.currentlyDisplayedParticipants = this.logic.Competition.getParticipantListByClass(Participant.competitionClassEnum.YoungerOB);
            this.updateParticipantsList();

        }

        private void olderOBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.currentlyDisplayedParticipants = this.logic.Competition.getParticipantListByClass(Participant.competitionClassEnum.OlderOB);
            this.updateParticipantsList();

        }

        private void juniorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.currentlyDisplayedParticipants = this.logic.Competition.getParticipantListByClass(Participant.competitionClassEnum.Junior);
            this.updateParticipantsList();

        }

        private void womenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.currentlyDisplayedParticipants = this.logic.Competition.getParticipantListByClass(Participant.competitionClassEnum.Women);
            this.updateParticipantsList();

        }

        private void hunterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.currentlyDisplayedParticipants = this.logic.Competition.getParticipantListByClass(Participant.competitionClassEnum.Hunter);
            this.updateParticipantsList();

        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        //Christoffer: Eventhandler for columnclicking
        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            //If column clicked already matches sortcolumn
            if (e.Column == LVColumnSorter.SortColumn)
            {
                if (LVColumnSorter.OrderOfSort == SortOrder.Ascending)
                {
                    LVColumnSorter.OrderOfSort = SortOrder.Descending;
                }
                else
                {
                    LVColumnSorter.OrderOfSort = SortOrder.Ascending;
                }
            }

            //Set column clicked and sortorder
            else
            {
                LVColumnSorter.SortColumn = e.Column;
                LVColumnSorter.OrderOfSort = SortOrder.Ascending;
            }

            //Sort
            this.listView1.Sort();

            
                
        }

        public void showHelpText(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = this.helper.getHelpText(sender);

        }

        public void removeHelpText(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "";
        }

        public void listViewTip(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Participant list. Click on the column captions to sort the table after a certain column";
        }

        private void openToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(logic.ResultStringByClass(Participant.competitionClassEnum.Open));
        }

        private void youngerOBToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(logic.ResultStringByClass(Participant.competitionClassEnum.YoungerOB));
        }

        private void olderOBToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(logic.ResultStringByClass(Participant.competitionClassEnum.OlderOB));
        }

        private void juniorToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(logic.ResultStringByClass(Participant.competitionClassEnum.Junior));
        }

        private void womenToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(logic.ResultStringByClass(Participant.competitionClassEnum.Women));
        }

        private void hunterToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(logic.ResultStringByClass(Participant.competitionClassEnum.Hunter));
        }

       

        
    }

}
