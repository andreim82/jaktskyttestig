﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1.Validators
{
    class MainValidator
    {
        public MainValidator() { }

        //Christoffer: true eller false beroende på större än 0 men lika eller mindre än 12
        public static bool numberOfStationsValid(int number)
        {
            if (number > 0 && number <= 12)
            {
                return true;
            }
            return false;
        }

        //Christoffer: Metod som skickar tillbaka false och felmeddelande om stringen är tom
        public bool textBoxValidating(string box, out string errorMessage)
        {
            if (box.Length == 0)
            {
                errorMessage = "Please enter the competition name.";
                return false;
            }
            else
            {
                errorMessage = string.Empty;
                return true;
            }

        }

        //Christoffer: true eller false beroende på om värdet på inten är 0
        public bool numericUpDownValidatingNotZero(int value, out string errorMessage)
        {
            if (value == 0)
            {
                errorMessage = "Please enter number of stations.";
                return false;
            }
            else
            {
                errorMessage = string.Empty;
                return true;
            }
        }

        //Christoffer: True eller false beroende på om inten är över 12
        public bool numericUpDownValidatingMaxTwelve(int value, out string errorMessage)
        {
            if (value > 12)
            {
                errorMessage = "Maximum number of stations exceeded (max 12)";
                return false;
            }
            else
            {
                errorMessage = string.Empty;
                return true;
            }
        }
        //Christoffer: Undersöker om personnumret är skrivet i rätt format
        public bool personalNumberRightFormat(string number, out string errorMessage)
        {
            if (number.IndexOf('-') == 6 || number.Length == 11)
            {
                try
                {
                    string part1 = number.Substring(0, 6);
                    string part2 = number.Substring(7, 4);

                    errorMessage = string.Empty;
                    return true;
                }

                catch (Exception)
                {
                    errorMessage = "Please check the personal number again.";
                    return false;

                }
            }
            else
            {
                errorMessage = "Personal number not of the right format (yymmdd-xxxx).";
                return false;
            }
        }

        //Christoffer: Undersöker om stringen endast består av integers
        public bool textBoxOfIntegers(string number, out string errorMessage)
        {
            try
            {
                int parsedNumber = Int32.Parse(number);
                errorMessage = string.Empty;
                return true;
            }
            catch (Exception)
            {
                errorMessage = "Enter a valid number value";
                return false;
            }
        }

        //Undersöker om stringen är max (int32)10
        public bool maxTenPoints(string number, out string errorMessage)
        {
            if (Convert.ToInt32(number) <= 10)
            {
                errorMessage = string.Empty;
                return true;
            }
            else
            {
                errorMessage = "Each target gives max 10 points";
                return false;
            }
        }
               
                

    }
}
