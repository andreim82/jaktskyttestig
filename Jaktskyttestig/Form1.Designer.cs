﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        /// 
        private void InitializeMyComponents()
        {
            this.LVColumnSorter = new Columnsorter.ListViewColumnSorter();
        }

        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startCardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.issueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.submitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewtoolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.currentlyShootingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.submittedStartCardsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.byClassToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.youngerOBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.olderOBToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.juniorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.womenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hunterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.currentResultToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.youngerOBToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.olderOBToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.juniorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.womenToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.hunterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.programHelpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listView1 = new System.Windows.Forms.ListView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.startCardToolStripMenuItem,
            this.viewtoolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(889, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.menuStrip1.MouseHover += new System.EventHandler(this.listViewTip);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(86, 20);
            this.toolStripMenuItem1.Text = "Competition";
            this.toolStripMenuItem1.ToolTipText = "Competition operations";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            this.toolStripMenuItem1.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.toolStripMenuItem1.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            this.newToolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.newToolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            this.openToolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.openToolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            this.saveToolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.saveToolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            this.exitToolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.exitToolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // startCardToolStripMenuItem
            // 
            this.startCardToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.issueToolStripMenuItem,
            this.submitToolStripMenuItem});
            this.startCardToolStripMenuItem.Name = "startCardToolStripMenuItem";
            this.startCardToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.startCardToolStripMenuItem.Text = "Start card";
            this.startCardToolStripMenuItem.Click += new System.EventHandler(this.startCardToolStripMenuItem_Click);
            this.startCardToolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.startCardToolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // issueToolStripMenuItem
            // 
            this.issueToolStripMenuItem.Name = "issueToolStripMenuItem";
            this.issueToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.issueToolStripMenuItem.Text = "Issue";
            this.issueToolStripMenuItem.Click += new System.EventHandler(this.issueToolStripMenuItem_Click);
            this.issueToolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.issueToolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // submitToolStripMenuItem
            // 
            this.submitToolStripMenuItem.Name = "submitToolStripMenuItem";
            this.submitToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.submitToolStripMenuItem.Text = "Submit";
            this.submitToolStripMenuItem.Click += new System.EventHandler(this.submitToolStripMenuItem_Click);
            this.submitToolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.submitToolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // viewtoolStripMenuItem
            // 
            this.viewtoolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.currentlyShootingToolStripMenuItem,
            this.submittedStartCardsToolStripMenuItem,
            this.byClassToolStripMenuItem,
            this.currentResultToolStripMenuItem});
            this.viewtoolStripMenuItem.Name = "viewtoolStripMenuItem";
            this.viewtoolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewtoolStripMenuItem.Text = "View";
            this.viewtoolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.viewtoolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(195, 22);
            this.toolStripMenuItem2.Text = "All Participants";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            this.toolStripMenuItem2.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.toolStripMenuItem2.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // currentlyShootingToolStripMenuItem
            // 
            this.currentlyShootingToolStripMenuItem.Name = "currentlyShootingToolStripMenuItem";
            this.currentlyShootingToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.currentlyShootingToolStripMenuItem.Text = "Currently Shooting";
            this.currentlyShootingToolStripMenuItem.Click += new System.EventHandler(this.currentlyShootingToolStripMenuItem_Click);
            this.currentlyShootingToolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.currentlyShootingToolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // submittedStartCardsToolStripMenuItem
            // 
            this.submittedStartCardsToolStripMenuItem.Name = "submittedStartCardsToolStripMenuItem";
            this.submittedStartCardsToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.submittedStartCardsToolStripMenuItem.Text = "Submitted Start Cards";
            this.submittedStartCardsToolStripMenuItem.Click += new System.EventHandler(this.submittedStartCardsToolStripMenuItem_Click);
            this.submittedStartCardsToolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.submittedStartCardsToolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // byClassToolStripMenuItem
            // 
            this.byClassToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem1,
            this.youngerOBToolStripMenuItem,
            this.olderOBToolStripMenuItem,
            this.juniorToolStripMenuItem,
            this.womenToolStripMenuItem,
            this.hunterToolStripMenuItem});
            this.byClassToolStripMenuItem.Name = "byClassToolStripMenuItem";
            this.byClassToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.byClassToolStripMenuItem.Text = "By Class";
            this.byClassToolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.byClassToolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // openToolStripMenuItem1
            // 
            this.openToolStripMenuItem1.Name = "openToolStripMenuItem1";
            this.openToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.openToolStripMenuItem1.Text = "Open";
            this.openToolStripMenuItem1.Click += new System.EventHandler(this.openToolStripMenuItem1_Click);
            // 
            // youngerOBToolStripMenuItem
            // 
            this.youngerOBToolStripMenuItem.Name = "youngerOBToolStripMenuItem";
            this.youngerOBToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.youngerOBToolStripMenuItem.Text = "Younger OB";
            this.youngerOBToolStripMenuItem.Click += new System.EventHandler(this.youngerOBToolStripMenuItem_Click);
            // 
            // olderOBToolStripMenuItem
            // 
            this.olderOBToolStripMenuItem.Name = "olderOBToolStripMenuItem";
            this.olderOBToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.olderOBToolStripMenuItem.Text = "Older OB";
            this.olderOBToolStripMenuItem.Click += new System.EventHandler(this.olderOBToolStripMenuItem_Click);
            // 
            // juniorToolStripMenuItem
            // 
            this.juniorToolStripMenuItem.Name = "juniorToolStripMenuItem";
            this.juniorToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.juniorToolStripMenuItem.Text = "Junior";
            this.juniorToolStripMenuItem.Click += new System.EventHandler(this.juniorToolStripMenuItem_Click);
            // 
            // womenToolStripMenuItem
            // 
            this.womenToolStripMenuItem.Name = "womenToolStripMenuItem";
            this.womenToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.womenToolStripMenuItem.Text = "Women";
            this.womenToolStripMenuItem.Click += new System.EventHandler(this.womenToolStripMenuItem_Click);
            // 
            // hunterToolStripMenuItem
            // 
            this.hunterToolStripMenuItem.Name = "hunterToolStripMenuItem";
            this.hunterToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.hunterToolStripMenuItem.Text = "Hunter";
            this.hunterToolStripMenuItem.Click += new System.EventHandler(this.hunterToolStripMenuItem_Click);
            // 
            // currentResultToolStripMenuItem
            // 
            this.currentResultToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem2,
            this.youngerOBToolStripMenuItem1,
            this.olderOBToolStripMenuItem1,
            this.juniorToolStripMenuItem1,
            this.womenToolStripMenuItem1,
            this.hunterToolStripMenuItem1});
            this.currentResultToolStripMenuItem.Name = "currentResultToolStripMenuItem";
            this.currentResultToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.currentResultToolStripMenuItem.Text = "Current Result By Class";
            this.currentResultToolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.currentResultToolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // openToolStripMenuItem2
            // 
            this.openToolStripMenuItem2.Name = "openToolStripMenuItem2";
            this.openToolStripMenuItem2.Size = new System.Drawing.Size(138, 22);
            this.openToolStripMenuItem2.Text = "Open";
            this.openToolStripMenuItem2.Click += new System.EventHandler(this.openToolStripMenuItem2_Click);
            // 
            // youngerOBToolStripMenuItem1
            // 
            this.youngerOBToolStripMenuItem1.Name = "youngerOBToolStripMenuItem1";
            this.youngerOBToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.youngerOBToolStripMenuItem1.Text = "Younger OB";
            this.youngerOBToolStripMenuItem1.Click += new System.EventHandler(this.youngerOBToolStripMenuItem1_Click);
            // 
            // olderOBToolStripMenuItem1
            // 
            this.olderOBToolStripMenuItem1.Name = "olderOBToolStripMenuItem1";
            this.olderOBToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.olderOBToolStripMenuItem1.Text = "Older OB";
            this.olderOBToolStripMenuItem1.Click += new System.EventHandler(this.olderOBToolStripMenuItem1_Click);
            // 
            // juniorToolStripMenuItem1
            // 
            this.juniorToolStripMenuItem1.Name = "juniorToolStripMenuItem1";
            this.juniorToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.juniorToolStripMenuItem1.Text = "Junior";
            this.juniorToolStripMenuItem1.Click += new System.EventHandler(this.juniorToolStripMenuItem1_Click);
            // 
            // womenToolStripMenuItem1
            // 
            this.womenToolStripMenuItem1.Name = "womenToolStripMenuItem1";
            this.womenToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.womenToolStripMenuItem1.Text = "Women";
            this.womenToolStripMenuItem1.Click += new System.EventHandler(this.womenToolStripMenuItem1_Click);
            // 
            // hunterToolStripMenuItem1
            // 
            this.hunterToolStripMenuItem1.Name = "hunterToolStripMenuItem1";
            this.hunterToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.hunterToolStripMenuItem1.Text = "Hunter";
            this.hunterToolStripMenuItem1.Click += new System.EventHandler(this.hunterToolStripMenuItem1_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.programHelpToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.helpToolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.helpToolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // programHelpToolStripMenuItem
            // 
            this.programHelpToolStripMenuItem.Name = "programHelpToolStripMenuItem";
            this.programHelpToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.programHelpToolStripMenuItem.Text = "Program Help";
            this.programHelpToolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.programHelpToolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.MouseLeave += new System.EventHandler(this.removeHelpText);
            this.aboutToolStripMenuItem.MouseHover += new System.EventHandler(this.showHelpText);
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(12, 27);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(865, 299);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listView1_ColumnClick);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 346);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(889, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Click += new System.EventHandler(this.toolStripStatusLabel1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 368);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Jaktskyttestig";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem programHelpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startCardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem issueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem submitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewtoolStripMenuItem;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem currentlyShootingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem submittedStartCardsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem byClassToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem youngerOBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem olderOBToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem juniorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem womenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hunterToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Columnsorter.ListViewColumnSorter LVColumnSorter;
        private System.Windows.Forms.ToolStripMenuItem currentResultToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem youngerOBToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem olderOBToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem juniorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem womenToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem hunterToolStripMenuItem1;

    }
}

