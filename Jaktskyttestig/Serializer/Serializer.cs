﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using WindowsFormsApplication1.Models;

namespace WindowsFormsApplication1.Serializer
{
    //Andrei: This class is used for disk operations: to read and write Competition objects 
    class Serializer
    {
        // This method takes a Competition object and writes it to a binary file
        public static void writeCompetition(string file, Competition competition)
        {
            try
            {
                //opening a stream
                Stream stream = File.Open(file, FileMode.Create);
                //creating BinaryFormatter object to convert Competition to Binary
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                //transferring the Competition object instance to binary stream
                binaryFormatter.Serialize(stream, competition);
                //closing the stream when the operation is ready
                stream.Close();
            }
             // If there's an error while saving file, the program prints an error message on the
            catch (IOException e)
            {
                Console.WriteLine("Exception occured while saving the file: {0}", e.GetType().Name);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("SerializationException occured while saving the file: {0}", e.GetType().Name);

            }
        }

        // this method reads a previously saved binary file with a competition and converts it to Competition object instance in the memory
        public static Competition readCompetition(string file)
        {
            Competition cmp = new Competition(1);
            try
            {
                //opening stream
                Stream stream = File.Open(file, FileMode.Open);
                //converting Competition from binary
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                cmp = (Competition)binaryFormatter.Deserialize(stream);

                // closing the stream
                stream.Close();
            }

                // Exceptions are raised if an error occurs during the read operation.
            catch (IOException e)
            {
                Console.WriteLine("Exception occured while reading the file: {0}", e.GetType().Name);

            }
            catch (SerializationException e)
            {
                Console.WriteLine("SerializationException occured while reading the file: {0}", e.GetType().Name);

            }
            // Competition object is returned
            return cmp;

        }

    }
}
