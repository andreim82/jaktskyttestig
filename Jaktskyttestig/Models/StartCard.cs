﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;

namespace WindowsFormsApplication1.Models
{
    [Serializable()]
    public class StartCard
    {
        private List<Station> stations;
        private Participant participant;
        private bool signed;

        public StartCard(int numberOfStations, Participant participant)
        {
            this.participant = participant;
            this.stations = new List<Station>();
            for (int i = 0; i < numberOfStations; i++)
            {
                this.stations.Add(new Station());
            }
        }

        public int getStationCount()
        {
            return this.stations.Count;
        }

        public Station getStationByNumber(int number)
        {
            return this.stations[number];
        }

        public List<Station> Stations
        {
            get
            {
                return this.stations;
            }
            set
            {
                this.stations = value;
            }
        }
        public bool Signed
        {
            get
            {
                return this.signed;
            }
            set
            {
                this.signed = value;
            }
        }
    }
}
