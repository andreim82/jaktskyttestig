﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;

namespace WindowsFormsApplication1.Models
{

    [Serializable()]
    public class Competition
    {
        private int stations = 0;
        private string name = "";
        private List<Participant> participants = new List<Participant>();

        public Competition(int s)
        {
            this.stations = s;
            this.participants = new List<Participant>();
        }

        /*public List<Participant> ResultList()
        {
            List<Participant> resultsort = new List<Participant>();
            int i;
            int j;
            Participant temp;

            for (i = (resultsort.Count() - 2); i >= 0; i--)
            {
                for (j = 1; j <= i; j++)
                {
                    if (resultsort[j - 1].TotalResult() > resultsort[j].TotalResult())
                    {
                        temp = resultsort[j - 1];
                        resultsort[j - 1] = resultsort[j];
                        resultsort[j] = temp;
                    }
                }
            }

            for (i = (resultsort.Count() - 2); i >= 0; i--)
            {
                for (j = 1; j <= i; j++)
                {
                    if (resultsort[j - 1].TotalResult() == resultsort[j].TotalResult())
                    {
                        if (resultsort[j - 1].TotalHits() > resultsort[j].TotalHits())
                        {
                            temp = resultsort[j - 1];
                            resultsort[j - 1] = resultsort[j];
                            resultsort[j] = temp;
                        }
                    }
                }
            }

            return resultsort;
        }*/

        // Andrei: when a StartCard is submitted, it gets associated with a certain Participant
        public void submitStartCard(StartCard c, Participant p)
        {
            p.SubmittedStartCard = true;
            p.StartCard = c;
        }

        public void addParticipant(Participant p)
        {
            this.participants.Add(p);
        }

        public int getNumberOfStations()
        {
            
            return this.stations;
        }

        // Andrei: this method is used by listView1 view filters. 
        // Using this method you can easily get a list of participants of a given class
        public List<Participant> getParticipantListByClass(Participant.competitionClassEnum competitorClass)
        {
            List<Participant> result = new List<Participant>();
            for (int i = 0; i < this.Participants.Count; i++)
            {
                if (this.participants[i].CompetitionClass == competitorClass)
                {
                    result.Add(this.participants[i]);
                }
            }
            return result;
        }


        public int getNumberOfParticipants()
        {
            if (this.participants == null)
            {
                return 0;
            }
            else
            {
                return this.participants.Count;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
           
        }
        public int Stations
        {
            get
            {
                return this.stations;
            }
            set
            {
                this.stations = value;
            }
        }
        //Andrei: getter/setter for Participants
        public List<Participant> Participants
        {
            get
            {
                return this.participants;
            }
            set
            {
                this.participants = value;
            }
        }
    }
}
