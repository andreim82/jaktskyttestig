﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;

namespace WindowsFormsApplication1.Models
{
    [Serializable()]
    public class Station
    {
        private int target1;
        private int target2;
        private int total;

        public Station()
        {
            this.target1 = 0;
            this.target2 = 0;
        }

        public int Target1Result
        {
            get
            {
                return this.target1;
            }
            set
            {
                this.target1 = value;
            }

        }
        public int Target2Result
        {
            get
            {
                return this.target2;
            }
            set
            {
                this.target2 = value;
            }
        }

        public int Total
        {
            get
            {
                return this.total;
            }
            set
            {
                this.total = value;
            }
        }
    }
}
