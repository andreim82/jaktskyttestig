﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;

namespace WindowsFormsApplication1.Models
{
    [Serializable()]
    public class Participant
    {
        public enum competitionClassEnum { Open, YoungerOB, OlderOB, Junior, Women, Hunter };
        private int startNumber;

        //private static int participantCount = 0;
        private string name = "";
        private string personalNumber = "";
        private string phoneNumber = "";
        private competitionClassEnum competitionClass;
        private bool receivedStartCard = false;
        private bool submittedStartCard = false;
        private StartCard startCard;

        public Participant(string n, string pn, string ph, competitionClassEnum c, int sn)
        {
            //participantCount++;
            this.startNumber = sn;
            this.name = n;
            this.personalNumber = pn;
            this.phoneNumber = ph;
            this.competitionClass = c;
            this.receivedStartCard = true;
            this.submittedStartCard = false;
        }

        //Christoffer: returnerar totala poängen för deltagaren
        public int TotalResult()
        {
            int totalresult = 0;
            foreach (Station i in startCard.Stations)
            {
                totalresult = totalresult + i.Target1Result + i.Target2Result;
            }

            return totalresult;
        }

        //Christoffer: Returnerar totalt antal träffar för deltagaren
        public int TotalHits()
        {
            int totalhits = 0;
            foreach (Station i in startCard.Stations)
            {
                totalhits = totalhits + i.Total;
            }

            return totalhits;
        }

        public int StartNumber
        {
            get
            {
                return this.startNumber;
            }
            set
            {
                this.startNumber = value;
            }

        }

        public StartCard StartCard
        {
            get
            {
                return this.startCard;
            }
            set
            {
                this.startCard = value;
            }
        }

        public bool ReceivedStartCard
        {
            get
            {
                return this.receivedStartCard;
            }
            set
            {
                this.receivedStartCard = value;
            }
        }


        public bool SubmittedStartCard
        {
            get
            {
                return this.submittedStartCard;
            }
            set
            {
                this.submittedStartCard = value;
            }
        }


        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public string PersonalNumber
        {
            get
            {
                return this.personalNumber;
            }
            set
            {
                this.personalNumber = value;

            }

        }

        public string PhoneNumber
        {
            get
            {
                return this.phoneNumber;
            }
            set
            {
                this.phoneNumber = value;
            }

        }

        public competitionClassEnum CompetitionClass
        {
            get
            {
                return this.competitionClass;
            }
            set
            {
                this.competitionClass = value;
            }
        }

    }
}
