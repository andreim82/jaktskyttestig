﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace WindowsFormsApplication1.Helpers
{
    class InteractiveHelper
    {
        Hashtable helpStrings = new Hashtable();
        public InteractiveHelper()
        {
            initHelpStrings();
        }

        public void initHelpStrings()
        {

            this.helpStrings.Add("Competition", "Creating new and saving competitions");
            this.helpStrings.Add("New", "Create a new competition");
            this.helpStrings.Add("Open", "Open saved competition");
            this.helpStrings.Add("Save", "Save current competition");
            this.helpStrings.Add("Exit", "Close the program");

            this.helpStrings.Add("Start card", "Issuing and submitting start cards");
            this.helpStrings.Add("Issue", "Create a new start card and give it to a participant");
            this.helpStrings.Add("Submit", "Register a submitted start card");

            this.helpStrings.Add("View", "Show participant lists by class, status etc.");
            this.helpStrings.Add("All Participants", "Show all registered participants");
            this.helpStrings.Add("Currently Shooting", "Show participants currently shooting");
            this.helpStrings.Add("Submitted Start Cards", "Show participants who already have submitted their results");
            this.helpStrings.Add("By Class", "Show from a certain class such as Junior, Women, Hunter etc...");
            this.helpStrings.Add("Current Result By Class", "Choose a class to show current results");
            
            this.helpStrings.Add("Help", "About the program and how to use it");
            this.helpStrings.Add("Program Help", "Instructions how to use the program");
            this.helpStrings.Add("About", "Short information about the program and the authors");

        }

        public string getHelpText(object o)
        {
            string key = o.ToString();

            if (this.helpStrings.Contains(key))
            {
                return this.helpStrings[key].ToString();
            }
            else
            {
                return "";
            }
        }

    }
}
