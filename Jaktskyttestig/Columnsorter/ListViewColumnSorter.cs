﻿using System;
using System.Collections;
using System.Windows.Forms;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication1.Columnsorter
{
    //Christoffer: Class for sorting columns in listviewer
    class ListViewColumnSorter : IComparer
    {
        private int columnToSort;
        private SortOrder orderOfSort;
        private CaseInsensitiveComparer objectCompare;

        public ListViewColumnSorter()
        {
            columnToSort = 0;
            orderOfSort = SortOrder.None;
            objectCompare = new CaseInsensitiveComparer();
        }

        //Compares one object to another
        public int Compare(object x, object y)
        {
            int compareResult;
            ListViewItem listVX, listVY;

            listVX = (ListViewItem)x;
            listVY = (ListViewItem)y;

            //Compares two objects using Compare from IComparer
            compareResult = objectCompare.Compare(listVX.SubItems[columnToSort].Text, listVY.SubItems[columnToSort].Text);

            //gives compareResult negative or positive value depending on ascending or descending sortorder
            if (orderOfSort == SortOrder.Ascending)
            {
                return compareResult;
            }
            else if (orderOfSort == SortOrder.Descending)
            {
                return (-compareResult);
            }
            else
            {
                return 0;
            }
        }

        public int SortColumn
        {
            set
            {
                columnToSort = value;
            }
            get
            {
                return columnToSort;
            }
        }

        public SortOrder OrderOfSort
        {
            set
            {
                orderOfSort = value;
            }
            get
            {
                return orderOfSort;
            }
        }

    }
}
