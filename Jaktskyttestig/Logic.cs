﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using WindowsFormsApplication1.Models;
using WindowsFormsApplication1.Serializer;
namespace WindowsFormsApplication1
{
    public class Logic
    {
        private Competition competition;
        private string competitionFile = "";
        public Logic() {}

        //Christoffer: Lagt till namnparameter
        public void createNewCompetition(int s, string n)
        {
            this.competition = new Competition(s);
            this.competition.Name = n;
        }

        public void addNewParticipant(string name, string personalNumber, string phoneNumber, Participant.competitionClassEnum competitorClass)
        {
            int nextStartNumber = this.competition.getNumberOfParticipants()+1;
            this.competition.addParticipant(new Participant(name, personalNumber, phoneNumber, competitorClass, nextStartNumber));
        }

        // Andrei: this method returns a status line depending on what happens in the program's internal logic
        public string getStatusLine()
        {
            if (this.competition == null)
            {
                return "Create a competition first before adding participants";
            }
            else if (this.competition != null && this.getNonSubmittedStartCardParticipants().Count == 0)
            {
                return "Issue startcards to new participants";
            }
            else if (this.competition != null && this.getNonSubmittedStartCardParticipants().Count > 0)
            {
                return "Issue more startcards to new participants or submit the issued ones";
            }

            return "";
        }

        //Andrei: this method loads a competition file
        public void openFile(string competitionFile)
        {
            this.competition = Serializer.Serializer.readCompetition(competitionFile);
            this.competitionFile = competitionFile;

        }

        //Andrei: this method saves a competition file
        public void saveFile(string competitionFile)
        {
            Serializer.Serializer.writeCompetition(competitionFile, this.competition);
            this.competitionFile = competitionFile;
        }

        // Andrei: this method returns those participants who already have submitted their StartCards
        public List<Participant> getSubmittedStartCardParticipants()
        {
            List<Participant> list = new List<Participant>();
            if (this.competition != null)
            {
                for (int i = 0; i < this.competition.getNumberOfParticipants(); i++)
                {
                    if (this.competition.Participants[i].SubmittedStartCard == true)
                    {
                        list.Add(this.competition.Participants[i]);
                    }
                }
            }
            return list;

        }

        //Christoffer: Genererar en lista med resultat sorterat på både poäng och totalt antal träffar
        public List<Participant> ResultList(Participant.competitionClassEnum CurrentClass)
        {
            List<Participant> tempsort = getSubmittedStartCardParticipants();
            List<Participant> resultsort = new List<Participant>();
            int i;
            int j;
            Participant temp;

            //filtrerar ut de i den önskade klassen
            foreach (Participant p in tempsort)
            {
                if(p.CompetitionClass == CurrentClass)
                {
                    resultsort.Add(p);
                }
            }

            //Bubblesort på poäng
            for (i = (resultsort.Count() - 1); i >= 0; i--)
            {
                for (j = 1; j <= i; j++)
                {
                    if (resultsort[j - 1].TotalResult() < resultsort[j].TotalResult())
                    {
                        temp = resultsort[j - 1];
                        resultsort[j - 1] = resultsort[j];
                        resultsort[j] = temp;
                    }
                }
            }

            //Bubblesort på totalträffar om poängen är lika
            for (i = (resultsort.Count() - 1); i >= 0; i--)
            {
                for (j = 1; j <= i; j++)
                {
                    if (resultsort[j - 1].TotalResult() == resultsort[j].TotalResult())
                    {
                        if (resultsort[j - 1].TotalHits() < resultsort[j].TotalHits())
                        {
                            temp = resultsort[j - 1];
                            resultsort[j - 1] = resultsort[j];
                            resultsort[j] = temp;
                        }
                    }
                }
            }

            return resultsort;
        }

        //Christoffer: Returnerar en string med resultat för viss klass
        public string ResultStringByClass(Participant.competitionClassEnum a)
        {
            string displayresult = "Place No." + "\t" + "Name" + "\t\t" + "Total Points" + "\t" + "Total Hits" + "\r\n\r\n"; ;
            List<Participant> resultlist = this.ResultList(a);
            int place = 1;
            if (resultlist.Count() != 0)
            {
                foreach (Participant i in resultlist)
                {
                    displayresult = displayresult + Convert.ToString(place) + "\t" + i.Name + "\t" + Convert.ToString(i.TotalResult()) + "\t\t" + Convert.ToString(i.TotalHits()) + "\r\n";
                    place++;
                }

                return displayresult;
            }

            else
            {
                displayresult = "No results to show as of yet";
                return displayresult;
            }
        }

        // Andrei: This method returns those Participants who have not submitted their StartCards yet
        public List<Participant> getNonSubmittedStartCardParticipants()
        {
            List<Participant> list = new List<Participant>();
            if (this.competition != null)
            {
                for (int i = 0; i < this.competition.getNumberOfParticipants(); i++)
                {
                    if (this.competition.Participants[i].SubmittedStartCard == false)
                    {
                        list.Add(this.competition.Participants[i]);
                    }
                }
            }
            return list;
        }

        public void issueNewStartCard()
        {

        }

        public Competition Competition
        {
            get
            {
                return this.competition;
            }
            set
            {
                this.competition = value;
            }
        }
        public string CompetitionFile
        {
            get
            {
                return this.competitionFile;
            }
            set
            {
                this.competitionFile = value;
            }

        }
    }
}
