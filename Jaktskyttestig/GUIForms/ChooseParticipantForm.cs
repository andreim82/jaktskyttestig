﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WindowsFormsApplication1.Models;
using WindowsFormsApplication1.GUIForms;
namespace WindowsFormsApplication1.Models
{
    public partial class ChooseParticipantForm : Form
    {
        Logic logic;
        Form1 parentForm;
        List<Participant> participants;

        //Andrei: make this form interact with the main Logic and Form: give references to this class instance:
        public ChooseParticipantForm(Logic l, Form1 p, List<Participant> participantList)
        {
            InitializeComponent();
            this.logic = l;
            this.parentForm = p;
            this.participants = participantList;
            // Fill in the participant list:
            this.populateForm();

            // Update the form
            this.updateForm();
        }

        private void ChooseParticipantForm_Load(object sender, EventArgs e)
        {

        }

        // Andrei: this method updates the form. Should be used from all other method in this class, when needed.
        private void populateForm()
        {
            listBox1.Items.Clear();
            listBox1.DisplayMember = "Name";
            listBox1.Items.AddRange(this.participants.ToArray());
        }
        private void updateForm()
        {
            if (listBox1.SelectedIndex < 0)
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Participant p = this.participants[this.listBox1.SelectedIndex];
            SubmitStartCardForm s = new SubmitStartCardForm(this.logic, this.parentForm, p);
            this.Close();
            s.Show();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            updateForm();
        }
    }
}
