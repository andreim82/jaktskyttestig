﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WindowsFormsApplication1.Validators;

namespace WindowsFormsApplication1
{
    public partial class NewCompetitionForm : Form
    {
        Logic logic;
        Form1 parentForm;
        MainValidator valid;
        
        

        // Andrei: added the Logic and Form1 parameters: this form can now interact with the main form
        public NewCompetitionForm(Logic l, Form1 p)
        {
            InitializeComponent();
            this.logic = l;
            this.parentForm = p;
            this.valid = new MainValidator();
            
        }
        
        //######### Getmetoder ##########

        //Christoffer: Metod för att ta reda på tävlingens namn
        public string GetCompetitionName()
        {
            return this.textBox1.Text;
        }

        //Christoffer: Metod för att ta reda på antal stationer i tävlingen
        public int GetNumberOfStations()
        {
            return (int)this.numericUpDown1.Value;
        }


        //######### Buttonclicks ##########

        //Christoffer: Autovalidering inaktiverad för formen. All validering körs via 
        //validatechildren när button1 aktiveras. Om allt är ok skapas logikdelar och
        //formen stängs.
        private void button1_Click(object sender, EventArgs e)
        {
            //Christoffer: Körs endast om ValidateChildren returnerar true
            if (this.ValidateChildren())
            {

                this.logic.createNewCompetition(this.GetNumberOfStations(), this.GetCompetitionName());
                this.parentForm.updateMenuItems();
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        //######### Validating ########## 
        
        //Christoffer: Validerare som körs när ValidateChildren anropas
        private void textBox1_Validating(object sender, CancelEventArgs e)
        {
            //Christoffer: Anropar metod i MainValidator som ger bool- svar och 
            //ett felmeddelande
            string errorMessage = "";
            if (!valid.textBoxValidating(textBox1.Text, out errorMessage))
            {
                e.Cancel = true;
                textBox1.Select(0, textBox1.Text.Length);
                MessageBox.Show(errorMessage);
            }   
        }

        private void numericUpDown1_Validating(object sender, CancelEventArgs e)
        {
            string errorMessage = "";
            //Christoffer: Cancel blir true om numericupdown1 är mindre än 0 eller mer än 12
            if (!valid.numericUpDownValidatingNotZero((int)numericUpDown1.Value, out errorMessage) || !valid.numericUpDownValidatingMaxTwelve((int)numericUpDown1.Value, out errorMessage))
            {
                e.Cancel = true;
                numericUpDown1.Select(0, numericUpDown1.Text.Length);
                MessageBox.Show(errorMessage);
            }
        }
    }
}
