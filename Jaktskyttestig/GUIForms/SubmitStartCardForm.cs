﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WindowsFormsApplication1.Models;
using WindowsFormsApplication1.Validators;

namespace WindowsFormsApplication1.GUIForms
{
    public partial class SubmitStartCardForm : Form
    {
        Logic logic;
        Form1 parentForm;
        StartCard startCard;
        Participant participant;
        Station currentStation;
        MainValidator valid;

        public SubmitStartCardForm(Logic l, Form1 p, Participant participant)
        {
            InitializeComponent();
            this.logic = l;
            this.parentForm = p;
            this.participant = participant;
            this.startCard = new StartCard(this.logic.Competition.getNumberOfStations(), this.participant);
            this.populateForm();
            this.updateForm();
            this.valid = new MainValidator();
        }

        

        private void SubmitStartCardForm_Load(object sender, EventArgs e)
        {
            

        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.currentStation = this.startCard.getStationByNumber(this.listBox2.SelectedIndex);
            textBox1.Text = this.currentStation.Target1Result.ToString();
            textBox2.Text = this.currentStation.Target2Result.ToString();
            textBox3.Text = this.currentStation.Total.ToString();
            button1.Enabled = true;
            button3.Enabled = true;
            textBox1.Enabled = true;
            textBox2.Enabled = true;
            textBox3.Enabled = true;
            textBox1.Focus();
            textBox1.Select(0, textBox1.Text.Length);
        }

        // Andrei: Filling in the form with station rows
        private void populateForm()
        {
            listBox2.Items.Clear();
            
            for (int i = 0; i < this.logic.Competition.Stations; i++)
            {
                listBox2.Items.Add((i + 1).ToString());
            }
        }


        //########### Buttonclick ###################


        // Andrei: Clicking on submit submits the StartCard and associates it with the given participant in the logic objec
        private void button1_Click(object sender, EventArgs e)
        {
            if (ValidateChildren())
            {
                this.logic.Competition.submitStartCard(this.startCard, this.participant);
                this.parentForm.updateMenuItems();
                this.parentForm.updateParticipantsList();
                this.Dispose();
                this.Close();

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Christoffer: Adds the entered value for shooting results to the selected station
        private void button3_Click(object sender, EventArgs e)
        {
            string errorMessage;

            if (valid.textBoxValidating(textBox1.Text, out errorMessage) && valid.textBoxOfIntegers(textBox1.Text, out errorMessage) && valid.maxTenPoints(textBox1.Text, out errorMessage) &&
               valid.textBoxValidating(textBox2.Text, out errorMessage) && valid.textBoxOfIntegers(textBox2.Text, out errorMessage) && valid.maxTenPoints(textBox2.Text, out errorMessage) &&
                valid.textBoxValidating(textBox3.Text, out errorMessage) && valid.textBoxOfIntegers(textBox3.Text, out errorMessage))
            {
                this.startCard.Stations[this.listBox2.SelectedIndex].Target1Result = Convert.ToInt32(textBox1.Text);
                this.startCard.Stations[this.listBox2.SelectedIndex].Target2Result = Convert.ToInt32(textBox2.Text);
                this.startCard.Stations[this.listBox2.SelectedIndex].Total = Convert.ToInt32(textBox3.Text);
            }
            else
            {
                MessageBox.Show(errorMessage);
            }
        }

        // Andrei: this method will contain everything that needs to be done once the form changes
        private void updateForm()
        {
        }

        // Andrei: When Shot Result field changes:
        /*private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
            if (textBox1.Text.Equals(""))
            {
                textBox1.Text = "0";
            }
            int result = 0;
            //Andrei: catching exceptions is needed for the case when the user types in characters that cannot be parsed to int
            try 
            {
                result = int.Parse(textBox1.Text);
            }
            catch (Exception exc) {
                result = 0;
                MessageBox.Show("Shot result not of valid format" + "\r\n" + exc.ToString());
            }

            this.currentStation.ShotResult = result;
        }

        // Andrei: When Diff Result field changes:
        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            int result = 0;
            if (textBox2.Text.Equals(""))
            {
                textBox2.Text = "0";
            }
            try            //Andrei: catching exceptions is needed for the case when the user types in characters that cannot be parsed to inttry
            {
                result = int.Parse(textBox2.Text);
            }
            catch (Exception)
            {
                result = 0;
            }
            this.currentStation.DiffResult = result;
        }*/

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this.startCard.Signed = this.checkBox1.Checked;
        }


        //########## Validators ############

        //Christoffer: Validatorer som körs när ValidateChildren anropas
        private void checkBox1_Validating(object sender, CancelEventArgs e)
        {
            if (!checkBox1.Checked)
            {
                e.Cancel = true;
                MessageBox.Show("Must be signed before submitting.");
            }

        }

        private void textBox1_Validating(object sender, CancelEventArgs e)
        {
            string errorMessage;
            if(!valid.textBoxValidating(textBox1.Text, out errorMessage))
            {
                e.Cancel = true;
                textBox1.Select(0, textBox1.Text.Length);
                MessageBox.Show(errorMessage);
            }

            else if(!valid.textBoxOfIntegers(textBox1.Text, out errorMessage))
            {
                e.Cancel = true;
                textBox1.Select(0, textBox1.Text.Length);
                MessageBox.Show(errorMessage);
            }
        }

        private void textBox2_Validating(object sender, CancelEventArgs e)
        {
            string errorMessage;
            if(!valid.textBoxValidating(textBox1.Text, out errorMessage))
            {
                e.Cancel = true;
                textBox1.Select(0, textBox1.Text.Length);
                MessageBox.Show(errorMessage);
            }

            else if (!valid.textBoxOfIntegers(textBox2.Text, out errorMessage))
            {
                e.Cancel = true;
                textBox2.Select(0, textBox2.Text.Length);
                MessageBox.Show(errorMessage);
            }
        }

        private void button3_MouseHover(object sender, EventArgs e)
        {
            
        }

        private void textBox3_Validating(object sender, CancelEventArgs e)
        {
            string errorMessage;
            if (!valid.textBoxValidating(textBox3.Text, out errorMessage))
            {
                e.Cancel = true;
                textBox3.Select(0, textBox3.Text.Length);
                MessageBox.Show(errorMessage);
            }

            else if (!valid.textBoxOfIntegers(textBox3.Text, out errorMessage))
            {
                e.Cancel = true;
                textBox3.Select(0, textBox3.Text.Length);
                MessageBox.Show(errorMessage);
            }

        }

        


        

        

       
        
    }
}
