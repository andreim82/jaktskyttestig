﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WindowsFormsApplication1.Models;
using WindowsFormsApplication1.Validators;

namespace WindowsFormsApplication1.GUIForms
{
    public partial class NewStartCardForm : Form
    {
        Logic logic;
        Form1 parentForm;
        MainValidator valid;
        

        //Andrei: NewStartCardForm ska kunna interagera med logiken Logic och huvudformen Form1
        public NewStartCardForm(Logic l, Form1 p)
        {
            this.logic = l;
            this.parentForm = p;
            this.valid = new MainValidator();
            InitializeComponent();
        }

        //######## Getmetoder ############

        //Metod för att ta reda på tävlandes namn
        public string GetParticipantName()
        {
            return textBox1.Text;
        }

        //Metod för att ta reda på personnummer
        public string GetPersonalNumber()
        {
            return textBox2.Text;
        }

        //Metod för att ta reda på phonenumber
        public string GetPhoneNumber()
        {
            return textBox3.Text;
        }

        //Metod för att ta reda på valet av klass,
        //Andrei: Har ändrat typen till enum som redan finns: Participant.competitionClassEnum
        //Så slipper vi konvertera int, och det ser lite tydligare ut
        public Participant.competitionClassEnum GetParticipantClass()
        {
            if (radioButton1.Checked)
            {
                return Participant.competitionClassEnum.Open;
            }
            else if (radioButton2.Checked)
            {
                return Participant.competitionClassEnum.YoungerOB;
            }
            else if (radioButton3.Checked)
            {
                return Participant.competitionClassEnum.OlderOB;
            }
            else if (radioButton4.Checked)
            {
                return Participant.competitionClassEnum.Junior;
            }
            else if (radioButton5.Checked)
            {
                return Participant.competitionClassEnum.Women;
            }
            else
            {
                return Participant.competitionClassEnum.Hunter;
            }
        }


        //########### Buttonclicks ##################

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // Andrei: när knappen klickas, läggs ny deltagare till Competition
        private void button1_Click(object sender, EventArgs e)
        {
            //Christoffer: Körs endast om ValidateChildren returnerar true
            if (ValidateChildren())
            {
                this.logic.addNewParticipant(this.GetParticipantName(), this.GetPersonalNumber(), this.GetPhoneNumber(), this.GetParticipantClass());
                this.parentForm.CurrentlyDisplayedParticipants = this.logic.Competition.Participants;
                this.parentForm.updateParticipantsList();
                this.parentForm.updateMenuItems();
                this.Close();
            }
            
        }

        //############## Validators #######################

        //Christoffer: Validatorer som körs när ValidateChildren anropas
        private void textBox1_Validating(object sender, CancelEventArgs e)
        {
            //Christoffer: Anropar metod i MainValidator som ger bool- svar och 
            //ett felmeddelande
            string errorMessage = "";
            if (!valid.textBoxValidating(textBox1.Text, out errorMessage))
            {
                e.Cancel = true;
                textBox1.Select(0, textBox1.Text.Length);
                MessageBox.Show(errorMessage);
            }  
        }

        private void textBox2_Validating(object sender, CancelEventArgs e)
        {
            //Christoffer: Anropar metod i MainValidator som ger bool- svar och 
            //ett felmeddelande
            string errorMessage = "";
            if (!valid.textBoxValidating(textBox2.Text, out errorMessage))
            {
                e.Cancel = true;
                textBox2.Select(0, textBox2.Text.Length);
                MessageBox.Show(errorMessage);
            }
            else if(!valid.personalNumberRightFormat(textBox2.Text, out errorMessage))
            {
                e.Cancel = true;
                textBox2.Select(0, textBox2.Text.Length);
                MessageBox.Show(errorMessage);
            }
        }

        private void textBox3_Validating(object sender, CancelEventArgs e)
        {
            //Christoffer: Anropar metod i MainValidator som ger bool- svar och 
            //ett felmeddelande
            string errorMessage = "";
            if (!valid.textBoxValidating(textBox3.Text, out errorMessage))
            {
                e.Cancel = true;
                textBox3.Select(0, textBox3.Text.Length);
                MessageBox.Show(errorMessage);
            }  
        }
    }
}
